<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Laravel Simple Project

As part of the job interview process we need to test basic Laravel skills. So the project should be simple, but at the same time touch the majority of fundamentals.  We believe 2-3 days is more than enough to complete this project. The project will be made available via our GIT repo, please clone it and push your own changes to your own GIT repo and then please share with us.  All contact via developer@signaturegroup.com.au

With that in mind, here’s a project we have come up with.

Adminpanel to manage companies

Basically, a project to manage companies and their employees. Mini-CRM.

- Basic Laravel Auth: ability to log in as administrator;
- Use database seeds to create first user with email and password;
- CRUD functionality (Create / Read / Update / Delete) for two menu items: Companies and Employees;
- Companies DB table consists of these fields: Name (required), email, logo (minimum 100x100), website;
- Employees DB table consists of these fields: First name (required), Last name (required), Company (foreign key to Companies), email, phone;
- Use database migrations to create those schemas above;
- Store companies logos in app/public folder and make them accessible from public;
- Use basic Laravel resource controllers with default methods, index, create, store etc…;
- Use of soft deletes;
- Use Laravel’s validation function, using Request classes;
- Use Laravel’s pagination for showing Companies/Employees list, 10 entries per page;
- Use Laravel make:auth as default Bootstrap-based design theme, but remove ability to register;
- Create api response to display Companies and Employees